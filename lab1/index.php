<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
    <div class="wrapper">
        <header>
            <a href=""><img src="img/Logo_Polytech_rus_main.jpg" alt=""></a>
            <h2>Самостоятельная работа "Hello, World"</h2>
        </header>
        <main>
            <div>
                <?php
                    echo "Hello, World";
                ?>
            </div>
        </main>
        <footer>
            <p>Выполнил Самарин А.С. 211-323</p>
        </footer>
    </div>
</body>
</html>